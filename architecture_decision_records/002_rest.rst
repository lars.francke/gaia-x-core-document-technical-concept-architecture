ADR-002: REST as the Interface Technology for Federation Services
=================================================================

:adr-id: 002
:revnumber: 0.9
:revdate: 23-07-2020
:status: accepted
:author: Catalogue WP
:stakeholder: All Federation Services

Summary
-------

GAIA-X defines a set of Federation Services. These Services provide an API for
the development of client applications. This "client" could be an application
developed by a GAIA-X Participant or a user interface that is part of GAIA-X
itself.

For the internal consistency of GAIA-X and the Federation Services, a common
technology and design principles should be selected for the Federation Service's
API interface.

The ADR proposes REST (HTTP+JSON) as the interface technology and OpenAPI as the
interface definition language.

Discussion
----------

Many different protocols are in use for internet-based software interfaces.
Following is an (incomplete) overview listing only the most common technologies.

- SOAP Webservices (https://www.w3.org/TR/soap/)
- XML-RPC (http://xmlrpc.com/)
- REST (HTTP+JSON)
- Object-Oriented Interfaces (Corba, OPC UA, ...)
- Message-Oriented Interfaces (e.g. via Kafka, MQTT, AMQP, DDS, ...)

In principle, the Federation Services could be implemented using either of the
mentioned interface technologies. But there are further criteria besides the
core functionality to take into consideration. Selecting a widely used
technology with a proven track-record ensures that GAIA-X Participants have
developers with the skills to immediately make use of the Federation Services.
Furthermore, the established technologies are more likely to be widely supported
by the different programming languages and environments in the long-term.

From those criteria, the choice has fallen on a combination of REST (HTTP+JSON).
It is the mostly widely used technology for web-API development (also used by
the current hyperscalers) and has mature tooling.

Based on the choice of REST (HTTP+JSON), several competing formats exist for
expressing the interface definitions in a human and machine-readable format.

- OpenAPI (http://spec.openapis.org/oas/v3.0.3)
- RAML (https://raml.org/)
- Hydra (https://www.hydra-cg.com/)
- Custom Format for GAIA-X

Again, all these technologies could perform the task to a sufficient degree. We
select the most widely used alternative OpenAPI.

Decision Statements
-------------------

The GAIA-X Federation Services use JSON-Documents transferred via a HTTP/REST
API for their interfaces.

Special use cases (e.g. for streaming data) might receive an exemption to use
different interface technologies.

The interfaces of the Federation Services are specified in the OpenAPI
Specification (v3 or later). http://spec.openapis.org/oas/v3.0.3

Consequences
------------

Developers can reuse tools for the commonly used combination of HTTP+JSON for
the Federation Services.

Code stubs to interact with the Federation Services can be auto-generated from
the OpenAPI definitions for many programming environments.

The Work-Packages that define the interfaces of the Federation Services use
OpenAPI to define the interfaces in a human and machine-readable format.

Notes
-----

The REST API specifications are described in https://gitlab.com/gaia-x/gaia-x-core/gaia-x-core-document-technical-concept-architecture/-/blob/master/architecture_document/federation_services.rst

ADR References
--------------

* ADR-001: JSON-LD as the Exchange Format for Self-Descriptions

External References
-------------------

* [REST] https://en.wikipedia.org/wiki/Representational_state_transfer
* [OpenAPI] http://spec.openapis.org/oas/v3.0.3
