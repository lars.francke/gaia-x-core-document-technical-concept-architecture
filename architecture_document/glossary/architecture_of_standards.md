## Architecture of Standards

The Architecture of Standards (AoS) document defines a target for GAIA-X by analysing and integrating already existing standards for data, sovereignty and infrastructure components as well as which standards are supported.

### alias
- AoS

### references
- This definition was consolidated from GAIA-X documents
